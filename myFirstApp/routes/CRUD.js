var express = require('express');
var router = new express.Router();

const database = [
{
	id : 0,
	titre: "essai",
},
{
	id : 1,
	titre: "essai2",
}
];

//on récpere tous les films
router.get("films",function(req,res){
	res.send(database);
});

//on retrouve un film par ID 
router.get("/:id",function(req,res){
	const {id} = req.params;
	const titre = _.find(database,["id",id]);
	res.status(200).json({
	message: 'Film trouvé!', titre
	});
});

//ajout d'un nouveau film 
router.put('/',function(req,res)
{
const {film} = req.body;
const id = _.uniqueID(); 
database.push({film,id}); 
res.json({message: 'ajout de ${id}', titre: {film,id}});
});


//mise à jour film
router.post('/id',(req,res){
const {id} = req.params; 
const {film} = req.body;  
const filmToUpdate = _.find(database,["id",id]);
filmToUpdate.film = film; 
res.json({message: 'mise à jour finie'});
});

//supprimer
router.delete('/:id',(req,res){
const {id} = req.params; 
_.remove(database,["id",id]); 
res.json({message: "suppression finie "});
});

module.exports = router;
